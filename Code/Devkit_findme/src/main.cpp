#include <Arduino.h>

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>
#include <WiFi.h>

#define BUTTON 38
#define NOTIF_LED 23 // GPIO2 -- Builtin LED

#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID_RX "beb5483e-36e1-4688-b7f5-ea07361b26a8" //To receive buzzer on off command
#define CHARACTERISTIC_UUID_TX  "0972EF8C-7613-4075-AD52-756F33D4DA91"  //In progress

BLECharacteristic *characteristicTX;
hw_timer_t *timer = NULL; // create a hardware timer

/****** Global Variables ******/
bool deviceConnected = false;
volatile byte led_state = LOW;
volatile int blink_count;

/******************************** 
 * Function Declaration
 ********************************/
void blink_nonblocking(int);
void IRAM_ATTR onTimer();

/****** BLE Class USR Msg ******/
class CharacteristicCallbacks: public BLECharacteristicCallbacks {
     void onWrite(BLECharacteristic *characteristic) {
          std::string rxValue = characteristic->getValue();
          if (rxValue.length() > 0) {
 
               for (int i = 0; i < rxValue.length(); i++) {
                 Serial.print(rxValue[i]);
               }
               Serial.println();
              if(rxValue[0] == '0')
                digitalWrite(NOTIF_LED,LOW);
              else if(rxValue[0] == '1')
                digitalWrite(NOTIF_LED,HIGH);
          }
     }//onWrite
};

class ServerCallbacks: public BLEServerCallbacks {  //In progress
    void onConnect(BLEServer* pServer) {
      Serial.println("Connected");
      deviceConnected = true;
      blink_nonblocking(2);
    };
 
    void onDisconnect(BLEServer* pServer) {      
      Serial.println("Disconnected");
      deviceConnected = false;
      blink_nonblocking(4);
    }
};


void setup() {
  Serial.begin(115200);
  pinMode(NOTIF_LED, OUTPUT);
  while (!Serial);

  //Setting unique name based on MAC address
  String dev_name = "Iam-Here:" + WiFi.macAddress().substring(12);
  char temp_str[dev_name.length() + 1];
  dev_name.toCharArray(temp_str, dev_name.length() + 1);
  BLEDevice::init(temp_str);  // Create the BLE Device
  BLEServer *pServer = BLEDevice::createServer(); // Create the BLE Server
  pServer->setCallbacks(new ServerCallbacks());  //Set callback()

  BLEService *pService = pServer->createService(SERVICE_UUID);  // Create the BLE Service
  BLECharacteristic *CharacteristicRX = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID_RX,
                                         BLECharacteristic::PROPERTY_READ |
                                         BLECharacteristic::PROPERTY_WRITE
                                       );
                                       
  CharacteristicRX->setCallbacks(new CharacteristicCallbacks());  //In progress
  /*** Chara to Transmit location (in progress) ***/
  characteristicTX = pService->createCharacteristic(
                       CHARACTERISTIC_UUID_TX,
                       BLECharacteristic::PROPERTY_NOTIFY
                     );
 
  characteristicTX->addDescriptor(new BLE2902());
  characteristicTX->setValue("RSSI Initializing...");

  pService->start();  // Start the service
  // BLEAdvertising *pAdvertising = pServer->getAdvertising();  // this still is working for backward compatibility
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  BLEDevice::startAdvertising();

  
  /*******  Timer *********/
  timer = timerBegin(0, 80, true);

  /* Attach onTimer function to our timer */
  timerAttachInterrupt(timer, &onTimer, true);

  /* Set alarm to call onTimer function every second 1 tick is 1us
  => 1 second is 1000000us */
  /* Repeat the alarm (third parameter) */
  timerAlarmWrite(timer, 100000, true);
}


/************************************* 
 * Loop 
 *************************************/
void loop() {
}

/************************************* 
 * Function Definition
 *************************************/
void IRAM_ATTR onTimer()
{
  led_state = !led_state;
  digitalWrite(NOTIF_LED, led_state);
  if (!blink_count)
  {
    timerAlarmDisable(timer);
    digitalWrite(NOTIF_LED, LOW);
  }
  blink_count--;
}

void blink_nonblocking(int n)
{
  blink_count = 2 * n;
  timerAlarmEnable(timer);
}