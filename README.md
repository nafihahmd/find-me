# Find Me

This is a BLE based solution to locate our belongings indoor. The concept is to make low form factor, low power consuming tag that can be attached to our assets. This 'Find Me tag' will be able to turn on a buzzer included in the tag when a command is passed from a smartphone. The sound of the buzzer can be used to identify the location of the tag and hence our asset.